import React, { Component } from "react";
import DataTable from "./dataTable";

class Table extends Component {
    constructor(props) {
        super(props);

        let model = {
            headers: [
                { title: "Id", accessor: "id", index: 0 },
                {
                    title: "Profile",
                    accessor: "profile",
                    width: "80px",
                    index: 1,
                    cell: {
                        type: "image",
                        style: {
                            width: "50px"
                        }
                    }
                },
                { title: "Name", accessor: "name", width: "300px", index: 2 },
                { title: "Age", accessor: "age", index: 3 },
                { title: "Qualificaion", accessor: "qualificaion", index: 4 },
                {
                    title: "Rating",
                    accessor: "rating",
                    width: "200px",
                    index: 5,
                    cell: row => (
                        <div className="rating">
                            <div
                                style={{
                                    backgroundColor: "lightskyblue",
                                    textAlign: "center",
                                    height: "1.9em",
                                    width: (row / 5) * 201 + "px",
                                    margin: "3px 0 4px 0"
                                }}
                            >
                                {row}
                            </div>
                        </div>
                    )
                }
            ],
            data: [
                {
                    id: 1,
                    name: "a",
                    age: 29,
                    qualificaion: "A.com",
                    rating: 3,
                    profile:
                        "https://upload.wikimedia.org/wikipedia/commons/thumb/7/7e/Circle-icons-profile.svg/768px-Circle-icons-profile.svg.png"
                },
                {
                    id: 1,
                    name: "b",
                    age: 32,
                    qualificaion: "B.com",
                    rating: 5,
                    profile:
                        "https://upload.wikimedia.org/wikipedia/commons/thumb/7/7e/Circle-icons-profile.svg/768px-Circle-icons-profile.svg.png"
                },
                {
                    id: 1,
                    name: "c",
                    age: 32,
                    qualificaion: "C.com",
                    rating: 2,
                    profile:
                        "https://upload.wikimedia.org/wikipedia/commons/thumb/7/7e/Circle-icons-profile.svg/768px-Circle-icons-profile.svg.png"
                }
            ]
        };

        for (var i = 0; i <= 20; i++) {
            model.data.push({
                id: i,
                name: "name " + i,
                age: i + 18,
                qualificaion: "Graduate",
                rating: i % 2 ? 3 : 4,
                profile: "https://www.winhelponline.com/blog/wp-content/uploads/2017/12/user.png"
            });
        }

        this.state = model;
    }
    render() {
        return (
            <div>
                <DataTable
                    className="data-table"
                    title="USER PROFILES"
                    keyField="id"
                    pagination={{ 
                        enabled: true, 
                        pageLength: 5, 
                        type: "long" 
                    }}
                    width="100%"
                    headers={this.state.headers}
                    data={this.state.data}
                    noData="No records!"
                />
            </div>
        );
    }
}

export default Table;
