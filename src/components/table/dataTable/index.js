import React, { Component } from "react";
import ReactDOM from "react-dom";
import "./styles.css";
import Pagination from "../pagination";

class DataTable extends Component {
    _preSearchData = null;

    constructor(props) {
        super(props);
        this.state = {
            headers: props.headers,
            data: props.data,
            pageData: props.data,
            sortby: null,
            descending: null,
            search: false,
            pageLength: this.props.pagination.pageLength || 5,
            currentPage: 1
        };
        this.keyField = props.keyField || "id";
        this.noData = props.noData || "No records!";
        this.width = props.width || "100%";

        this.pagination = this.props.pagination || {};
    }
    onDragOver = e => {
        e.preventDefault();
    };
    onDragStart = (e, source) => {
        e.dataTransfer.setData("text/plain", source);
    };
    onDrop = (e, target) => {
        e.preventDefault();
        let source = e.dataTransfer.getData("text/plain");
        let headers = [...this.state.headers];
        let srcHeader = headers[source];
        let targetHeader = headers[target];
        let temp = srcHeader.index;
        srcHeader.index = targetHeader.index;
        targetHeader.index = temp;

        this.setState({
            headers
        });
    };
    renderTableHeader = () => {
        let { headers } = this.state;
        headers.sort((a, b) => {
            if (a.index > b.index) return 1;
            return -1;
        });
        let headerView = headers.map((header, index) => {
            let title = header.title;
            let cleanTitle = header.accessor;
            let width = header.width;

            if (this.state.sortby === index) {
                title += this.state.descending ? "\u2193" : "\u2191";
            }

            return (
                <th
                    key={cleanTitle}
                    ref={th => (this[cleanTitle] = th)} // this.id, this.profile, this.name
                    style={{ width: width }}
                    data-col={cleanTitle}
                    onDragStart={e => this.onDragStart(e, index)}
                    onDragOver={this.onDragOver}
                    onDrop={e => this.onDrop(e, index)}
                >
                    <span className="header-cell" data-col={cleanTitle} draggable>
                        {title}
                    </span>
                </th>
            );
        });

        return headerView;
    };
    renderNoData = () => {
        return (
            <tr>
                <td colSpan={this.props.headers.length}>{this.noData}</td>
            </tr>
        );
    };
    renderContent = () => {
        let { headers } = this.state;
        let data = this.pagination ? this.state.pageData : this.state.data;
        let contentView = data.map((row, rowIdx) => {
            let id = row[this.keyField];
            let tds = headers.map((header, index) => {
                let content = row[header.accessor];
                let cell = header.cell;
                if (cell) {
                    if (typeof cell === "object") {
                        if (cell.type === "image" && content) {
                            content = <img style={cell.style} src={content} />;
                        }
                    } else if (typeof cell === "function") {
                        content = cell(content);
                    }
                }
                return (
                    <td key={index} data-id={id} data-row={rowIdx}>
                        {content}
                    </td>
                );
            });

            return <tr key={rowIdx}>{tds}</tr>;
        });
        return contentView;
    };
    onSort = e => {
        let data = this.state.data.slice();
        let colIndex = ReactDOM.findDOMNode(e.target).parentNode.cellIndex;
        let colTitle = e.target.dataset.col;
        let descending = !this.state.descending;
        data.sort((a, b) => {
            let sortVal = 0;
            if (a[colTitle] < b[colTitle]) {
                sortVal = -1;
            } else if (a[colTitle] > b[colTitle]) {
                sortVal = 1;
            }
            if (descending) {
                sortVal = sortVal * -1;
            }
            return sortVal;
        });
        this.setState({
            data,
            sortby: colIndex,
            descending
        });
    };
    onSearch = e => {
        let needle = e.target.value.trim().toLowerCase();
        let { headers } = this.state;
        let idx = e.target.dataset.idx;
        let targetCol = this.state.headers[idx].accessor;
        let data = this._preSearchData;

        let searchData = this._preSearchData.filter(row => {
            let show = true;
            for (let field in row) {
                let fieldValue = row[field];
                let inputId = "inp" + field;
                let input = this[inputId];
                if (!fieldValue === "") {
                    show = true;
                } else {
                    show =
                        fieldValue
                            .toString()
                            .toLowerCase()
                            .indexOf(input.value.toLowerCase()) > -1;
                    if (!show) break;
                }
            }
            return show;
        });
        this.setState(
            {
                data: searchData,
                pageData: searchData,
                totalRecords: searchData.length
            },
            () => {
                if (this.pagination.enabled) {
                    this.onGotoPage(1);
                }
            }
        );
    };
    renderSearch = () => {
        let { search, headers } = this.state;
        if (!search) {
            return null;
        }
        let searchInputs = headers.map((header, idx) => {
            let hdr = this[header.accessor]; // get header ref
            let inputId = "inp" + header.accessor;

            return (
                <td key={idx}>
                    <input
                        ref={input => (this[inputId] = input)}
                        type="text"
                        data-idx={idx}
                        style={{
                            width: hdr.clientWidth - 20 + "px"
                        }}
                    />
                </td>
            );
        });
        return <tr onChange={this.onSearch}>{searchInputs}</tr>;
    };
    renderTable = () => {
        let title = this.props.title || "Data Table";
        let headerView = this.renderTableHeader();
        let contentView = this.state.data.length > 0 ? this.renderContent() : this.renderNoData();

        return (
            <table className="data-inner-table">
                <caption className="data-table-caption">{title}</caption>
                <thead onClick={this.onSort}>
                    <tr>{headerView}</tr>
                </thead>
                <tbody>
                    {this.renderSearch()}
                    {contentView}
                </tbody>
            </table>
        );
    };
    onToggleSearch = e => {
        if (this.state.search) {
            this.setState({
                data: this._preSearchData,
                search: false
            });
            this._preSearchData = null;
        } else {
            this._preSearchData = this.state.data;
            this.setState({
                search: true
            });
        }
    };
    renderToolbar = () => {
        return (
            <div className="toolbar">
                <button onClick={this.onToggleSearch}>Search</button>
            </div>
        );
    };
    getPageData = (pageNo, pageLength) => {
        let startOfRecord = (pageNo - 1) * pageLength;
        let endOfRecord = startOfRecord + pageLength;

        let data = this.state.data;
        let pageData = data.slice(startOfRecord, endOfRecord);

        return pageData;
    };
    onPageLengthChange = pageLength => {
        this.setState(
            {
                pageLength: parseInt(pageLength, 10)
            },
            () => {
                this.onGotoPage(this.state.currentPage);
            }
        );
    };
    onGotoPage = pageNo => {
        let pageData = this.getPageData(pageNo, this.state.pageLength);
        this.setState({
            pageData: pageData,
            currentPage: pageNo
        });
    };
    componentDidMount() {
        if (this.pagination) {
            this.onGotoPage(this.state.currentPage);
        }
    }
    render() {
        return (
            <div className={this.props.className}>
                {this.pagination.enabled && (
                    <Pagination
                        type={this.props.pagination.type}
                        totalRecords={this.state.data.length}
                        pageLength={this.state.pageLength}
                        onPageLengthChange={this.onPageLengthChange}
                        onGotoPage={this.onGotoPage}
                        currentPage={this.state.currentPage}
                    />
                )}

                {this.renderToolbar()}
                {this.renderTable()}
            </div>
        );
    }
}

export default DataTable;
