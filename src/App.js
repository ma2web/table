import React from 'react';
import './App.css';
import Table from './components/table/Table.jsx';

function App() {
  return (
    <div>
      <Table />
    </div>
  );
}

export default App;
